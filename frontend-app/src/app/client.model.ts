// client.model.ts
export interface Client {
  code: string;
  name: string;
  lastName: string;
  age: number;
  accountNumber: string;
  accountCreationDate: string;
  movements: Movement[]; // Cambiar esto a 
}

export interface Movement {
  type: string;
  amount: number;
  description: string;
}