// app-routing.module.ts

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ClientInfoComponent } from './client-info/client-info.component';

const routes: Routes = [
  { path: '', redirectTo: '/client-info', pathMatch: 'full' }, // Redirige a /client-info por defecto
  { path: 'client-info', component: ClientInfoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
