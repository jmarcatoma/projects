import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Client } from './client.model';

@Injectable({
  providedIn: 'root'
})
export class ClientService {

  private backendUrl = 'http://localhost:8080/restService/client/';

  constructor(private http: HttpClient) { }

  getClientByCode(code: string): Observable<Client> {
    console.log(this.http.get<Client>(`${this.backendUrl}${code}`));
    
    return this.http.get<Client>(`${this.backendUrl}${code}`);
  }
}
