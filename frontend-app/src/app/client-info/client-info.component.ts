import { Component } from '@angular/core';
import { Client, Movement } from '../client.model';
import { ClientService } from '../client.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-client-info',
  templateUrl: './client-info.component.html',
  styleUrls: ['./client-info.component.css']
})
export class ClientInfoComponent {
  client: Client | null = null;
  code: string = '';
  errorMessage: string = '';
  isCodeTooLong: boolean = false;
  isLoading: boolean = false; 

  constructor(private clientService: ClientService) {}

  getClientByCode(): void {
    this.isLoading = true;
    this.clientService.getClientByCode(this.code).subscribe(
      (response: Client) => {
        this.client = response;
        this.errorMessage = '';
        this.isLoading = false; 
      },
      (error) => {
        this.client = null; // Reiniciar el objeto del cliente
        this.errorMessage = 'Error al obtener la información del cliente: ' + error.message; // Mostrar el mensaje de error
        this.isLoading = false;
      }
    );
  }
  calculateBalance(client: Client): number {
    let balance = 0;
    for (const movement of client.movements) {
      if (movement.type === 'Crédito') {
        balance += movement.amount;
      } else if (movement.type === 'Débito') {
        balance -= movement.amount;
      }
    }
    return balance;
  }

  // Método para verificar la longitud del código al teclear
  checkCodeLength() {
    if (this.code.length > 9) {
      this.isCodeTooLong = true;
    } else {
      this.isCodeTooLong = false;
    }
  }
}